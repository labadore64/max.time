﻿namespace max.time.timer
{
    /// <summary>
    /// Represents a timer that includes FrameTimer and StopwatchTimer functionality.
    /// </summary>
    public class Timer : ITimer
    {
        /// <summary>
        /// The FrameTimer.
        /// </summary>
        /// <value>Frame Timer</value>
        public FrameTimer FrameTimer { get; private set; } = new FrameTimer();
        /// <summary>
        /// The StopwatchTimer.
        /// </summary>
        /// <value>Stopwatch Timer</value>
        public StopwatchTimer StopwatchTimer { get; private set; } = new StopwatchTimer();

        /// <summary>
        /// Whether or not you should use the stopwatch timer.
        /// </summary>
        /// <value>True if using Stopwatch Timer, False if using Frame Timer</value>
        public bool UseStopwatch { get { return _useStopwatch; } set { Reset(); _useStopwatch = value; } }
        bool _useStopwatch;

        /// <summary>
        /// Whether or not the timer is counting down.
        /// </summary>
        /// <value>True/False</value>
        public bool Active { get; set; }

        /// <summary>
        /// Resets the timer and sets it to inactive.
        /// </summary>
        public void Reset()
        {
            FrameTimer.Reset();
            StopwatchTimer.Reset();
        }

        /// <summary>
        /// Starts the timer and sets it to active, based on the value of UseStopwatch.
        /// </summary>
        public void Start()
        {
            if (Active)
            {
                if (_useStopwatch)
                {
                    StopwatchTimer.Start();
                }
                else
                {
                    FrameTimer.Start();
                }
            }
        }

        /// <summary>
        /// Updates the state of the timer for a frame cycle.
        /// </summary>
        public void Update()
        {
            if (Active)
            {
                if (!_useStopwatch)
                {
                    FrameTimer.Update();
                }
            }
        }

        /// <summary>
        /// Starts the stopwatch timer.
        /// </summary>
        public void StartStopwatch()
        {
            UseStopwatch = true;
            Start();
        }

        /// <summary>
        /// Starts the frame timer.
        /// </summary>
        public void StartFrameTimer()
        {
            UseStopwatch = false;
            Start();
        }

        /// <summary>
        /// Checks if the Stopwatch Timer falls between two times.
        /// </summary>
        /// <param name="StartTime">Start time (from 0) in ticks</param>
        /// <param name="EndTime">End time (from 0) in ticks</param>
        /// <returns>True/False</returns>
        public bool CheckTime(long StartTime, long EndTime)
        {
            return StopwatchTimer.CheckTime(StartTime, EndTime);
        }

        /// <summary>
        /// Checks if the Frame Timer falls between two frames.
        /// </summary>
        /// <param name="StartFrame">Start frame</param>
        /// <param name="EndFrame">End frame</param>
        /// <returns>True/False</returns>
        public bool CheckFrame(int StartFrame, int EndFrame)
        {
            return FrameTimer.CheckFrame(StartFrame, EndFrame);
        }
    }
}

﻿namespace max.time.timer
{
    /// <summary>
    /// This class is a timer that updates every frame.
    /// </summary>
    public class FrameTimer : ITimer
    {
        /// <summary>
        /// The current frame that the Update Timer is on.
        /// </summary>
        /// <value>Frame</value>
        public int Frame { get; private set; } = -1;

        /// <summary>
        /// Whether or not the timer is currently ticking down.
        /// </summary>
        /// <value>True/False</value>
        public bool Active { get; set; } = false;

        /// <summary>
        /// Updates the timer. Call this once per game update frame.
        /// </summary>
        public void Update()
        {
            if (Active)
            {
                Frame++;
            }
        }

        /// <summary>
        /// Start the timer.
        /// </summary>

        public void Start()
        {
            Active = true;
        }

        /// <summary>
        /// Resets the timer. In order to start the timer again
        /// you must set it to active = true.
        /// </summary>
        public void Reset()
        {
            Frame = -1;
            Active = false;
        }

        /// <summary>
        /// Check if the current frame is between the two provided frames.
        /// </summary>
        /// <param name="StartFrame">The start frame</param>
        /// <param name="EndFrame">The end frame</param>
        /// <returns>True/False</returns>

        public bool CheckFrame(int StartFrame, int EndFrame)
        {
            if (StartFrame != -1 && EndFrame != -1)
            {
                return StartFrame <= Frame && EndFrame >= Frame;
            }
            else
            {
                if (StartFrame != -1 || EndFrame != -1)
                {
                    if (StartFrame != -1)
                    {
                        return StartFrame <= Frame;
                    }
                    else if (EndFrame != -1)
                    {
                        return EndFrame >= Frame;
                    }
                }
                else
                {
                    return true;
                }
            }

            return false;
        }
    }
}

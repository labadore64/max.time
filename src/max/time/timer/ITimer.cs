﻿namespace max.time.timer
{
    /// <summary>
    /// This interface represents a timer.
    /// </summary>
    public interface ITimer
    {
        /// <summary>
        /// Whether or not the timer is actively counting down.
        /// </summary>
        /// <value>True/False</value>
        bool Active { get; } 

        /// <summary>
        /// Resets the timer state and sets it to inactive.
        /// </summary>
        void Reset();

        /// <summary>
        /// Starts the timer and sets it to active.
        /// </summary>
        void Start();
    }
}

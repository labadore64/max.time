﻿using System;

namespace max.time.timer
{
    /// <summary>
    /// This class is a timer that is based on the current System time.
    /// </summary>
    public class StopwatchTimer : ITimer
    {
        /// <summary>
        /// The time used to start the stopwatch.
        /// </summary>
        /// <value>Start Time</value>
        public long StartTime { get; private set; }

        /// <summary>
        /// The ticks passed since the StartTime was set.
        /// </summary>
        /// <value>Ticks Passed</value>
        public long TicksPassed
        {
            get
            {
                if(StartTime == 0)
                {
                    return 0;
                }
                return DateTime.Now.TimeOfDay.Ticks - StartTime;
            }
        }

        /// <summary>
        /// The milliseconds passed since the StartTime was set.
        /// </summary>
        /// <value>Time Passed</value>
        public long TimePassed
        {
            get
            {
                return (long)Math.Round(TicksPassed / (double)TimeSpan.TicksPerMillisecond);
            }
        }

        /// <summary>
        /// Whether or not the timer is currently ticking down.
        /// </summary>
        /// <value>True/False</value>
        public bool Active
        {
            get
            {
                return StartTime != 0;
            }
        }

        /// <summary>
        /// Sets the StartTime of the stopwatch to the current moment.
        /// </summary>
        public void Start()
        {
            StartTime = DateTime.Now.TimeOfDay.Ticks;
        }

        /// <summary>
        /// Resets the timer and sets it to inactive.
        /// </summary>
        public void Reset()
        {
            StartTime = 0;
        }

        /// <summary>
        /// Checks if the timer is between two times.
        /// </summary>
        /// <param name="StartTime">Start time (from 0) in ticks</param>
        /// <param name="EndTime">End time (from 0) in ticks</param>
        /// <returns>True/False</returns>
        public bool CheckTime(long StartTime, long EndTime)
        {
            long ticksPassed = TicksPassed;
            if (StartTime != 0 && EndTime != 0)
            {
                return StartTime <= ticksPassed && EndTime >= ticksPassed;
            }
            else
            {
                if (StartTime != 0 || EndTime != 0)
                {
                    if (StartTime != 0)
                    {
                        return StartTime <= ticksPassed;
                    }
                    else if (EndTime != 0)
                    {
                        return EndTime >= ticksPassed;
                    }
                }
                else
                {
                    return true;
                }
            }

            return false;
        }
    }
}

# Timer

The timer provides an interface to easily test timing.

## Working with FrameTimer/StopwatchTimer

It is possible to use only one of the two types of timers if your application needs it, however, Max Libraries standardize to using both types of timers. This is because this allows end users to choose between using Frame Timer and Stopwatch Timers for various input functions without having to modify code. Instead, it's recommended that you use ``Timer`` instead.

Please read [Measuring Time](TimeInfo.md) to learn more about ``FrameTimer`` and ``StopwatchTimer``.

## Working with Timer

The ``Timer`` class combines the StopwatchTimer and the FrameTimer into a single interface that allows you to switch between the two types of timers and test their states easily.

### Configuring the timer

First, instantiate a timer.

```
Timer Timer = new Timer();
```

If you want to switch the kind of timer that is being used, set the value ``UseStopwatch``.

```
// will use the Stopwatch Timer.
Timer.UseStopwatch = true;

// will use the Frame Timer.
Timer.UseStopwatch = false;
```

Every update cycle, you will need to call ``Update()`` to update the Timer's state.

```
Timer.Update();
```

### Using the Timer

To start the timer, call ``Start()``. It will start the timer depending on the value of ``UseStopwatch``.

```
Timer.Start();
```

You can also start specifically the Stopwatch Timer with ``StartStopwatch()`` and the Frame Timer with ``StartFrameTimer()`` repsectively.

To get the current time/frame of the timer, reference the values inside the individual timers, like this:

```
// get how much time has passed since being started.
int frame = 0;
long ticks = 0;

if(Timer.UseStopwatch){
	// get how many frames have passed.
	frame = Timer.FrameTimer.Frame;
} else {
	// get how many ticks have passed.
	ticks = Timer.StopwatchTimer.TicksPassed;
}
```

To check if the timer is currently between two times, use the ``CheckFrame()`` method for Frame Timers and ``CheckTime()`` for Stopwatch Timers. Note that ``CheckTime`` takes parameters in ticks.

```
if(Timer.UseStopwatch){
	if(Timer.CheckTime(0,20000000)){
		Console.WriteLine("Tested within the times.");
	}
} else {
	if(Timer.CheckFrame(0,50)){
		Console.WriteLine("Tested within the frames.");
	}
}
```

To reset the timer, call ``Reset()``.

```
Timer.Reset();
```
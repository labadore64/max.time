# Measuring Time

Time can be measured by the timer in two ways - by comparing system times in the Stopwatch Timer, or by comparing frames in the Frame Timer.

## Stopwatch Timer

The ``StopwatchTimer`` is a timer that, when started, records the current system time. This time is stored as type ``long`` in the variable ``StartTime``, and ``TicksPassed`` contains how many ticks have passed since ``StartTime``. When the current time is tested with the ``CheckTime`` method, it will retrieve the current system time, and compare it wil the start time, and see if its within acceptable range.

Note that because the timer retrieves the system time when it's called, if you repeatedly reference the time, they will have different times. Depending on what your needs are, you may need to store the value in a variable for method computation.

## Frame Timer

The ``FrameTimer`` is a timer that counts frames. This frame count is stored as type ``int`` in the variable ``Frame``. When the timer is started, it will count a single frame every time ``Update`` is called. You should call ``Update`` once per application cycle. When the current time is tested with the ``CheckFrame`` method, it will retrieve the current frame number and see if its within acceptable range.

# Using max.time

``max.time`` is a MaxLib Helper Library that includes timer classes and utilities relating to time.

* [Installation](Installation.md)
* [Measuring Time](TimeInfo.md)
* [Timer](Timers.md)

## Max Dependencies:

``max.time`` does not depend on any other Max libraries.
# max.time

``max.time`` is a MaxLib Helper Library that includes timer classes and utilities relating to time.

* [Documentation](https://labadore64.gitlab.io/max.time/)
* [Source](https://gitlab.com/labadore64/max.time/)
